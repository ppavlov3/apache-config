import subprocess
import sys


def test_conf():
    subprocess.check_call(["cp", "vhosts.conf", "vhosts_copy.conf"])
    subprocess.check_call([sys.executable, "conf.py"])
    with open("vhosts.conf") as f:
        with open("vhosts_copy.conf") as f_copy:
            assert f.read() == f_copy.read(),\
                "generated vhosts.conf differs from the provided vhosts.conf"


def test_template():
    with open("vhosts.j2") as f:
        content = f.read()
        assert "{% endfor %}" in content, "Please add for-loop"
        assert "{% endif %}" in content, "Please add if-statement"
